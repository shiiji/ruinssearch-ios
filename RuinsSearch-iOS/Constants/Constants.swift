//
//  Constants.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/19.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit

let LMN_UPDATED:Notification.Name = Notification.Name("LOCATION_MANAGER_UPDATE_LOCATION")

let SETTING_UPDATE:Notification.Name = Notification.Name("MENU_ITEM_SETTING_UPDATED")

let KNOCK_STARTED:Notification.Name = Notification.Name("KNOCK_MANAGER_KNOCK_STARTED")
let KNOCK_UPDATED:Notification.Name = Notification.Name("KNOCK_MANAGER_KNOCK_UPDATED")
let KNOCK_FINISHED:Notification.Name = Notification.Name("KNOCK_MANAGER_KNOCK_FINISHED")
