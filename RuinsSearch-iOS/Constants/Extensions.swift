//
//  Extensions.swift
//  BocchiDrive
//
//  Created by sera on 2017/04/08.
//  Copyright © 2017年 sera. All rights reserved.
//
import Foundation
import CoreLocation
import CocoaLumberjackSwift


// MARK: NSObject/Notification Extension
extension NSObject {
    func dd(_ section:String,_ message:String,_ args:CVarArg...) {
        #if DEBUG
            DDLogDebug(ddformat(section, message, args))
        #endif
    }
    
    func ddv(_ section:String,_ message:String,_ args:CVarArg...) {
        #if DEBUG
        DDLogVerbose(ddformat(section, message, args))
        #endif
    }
    
    func dde(_ section:String,_ message:String,_ args:CVarArg...) {
        #if DEBUG
        DDLogError(ddformat(section, message, args))
        #endif
    }
    
    func ddi(_ section:String,_ message:String,_ args:CVarArg...) {
        #if DEBUG
        DDLogInfo(ddformat(section, message, args))
        #endif
    }
    
    func ddw(_ section:String,_ message:String,_ args:CVarArg...) {
        #if DEBUG
        DDLogWarn(ddformat(section, message, args))
        #endif
    }
    
    func ddformat(_ section:String, _ message:String, _ args:[CVarArg]) -> String {
        var text = String(format: "[%@]", Date().string()) + ":" + String(format: "[%@]", section) + " "
        text = text + String(format: message, arguments: args)
        return text
    }
    
}

// MARK: Dictionary/Marge Extension
extension Dictionary {
    mutating func merge<S: Sequence>(contentsOf other: S) where S.Iterator.Element == (key: Key, value: Value) {
        for (key, value) in other {
            self[key] = value
        }
    }
    
    func merged<S: Sequence>(with other: S) -> [Key: Value] where S.Iterator.Element == (key: Key, value: Value) {
        var dic = self
        dic.merge(contentsOf: other)
        return dic
    }
}

// MARK: Array
extension Array {
    
}

// MARK: String
extension String {
    //絵文字など(2文字分)も含めた文字数を返します
    var count: Int {
        let string_NS = self as NSString
        return string_NS.length
    }
    
    func index(of string:String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    
    func endIndex(of string:String, options:CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    
    func indexes(of string:String, options: CompareOptions = .literal) -> [Index] {
        var result:[Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
    
    func contains(s:String) -> Bool {
        return index(of: s) != nil
    }
    
    func replace(_ target: String, _ withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString)
    }
    
    // 標準的なトリム
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    //正規表現の検索をします
    func pregMatche(pattern: String, options: NSRegularExpression.Options = []) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: options) else {
            return false
        }
        let matches = regex.matches(in: self, options: [], range: NSMakeRange(0, self.count))
        return matches.count > 0
    }
    
    //正規表現の検索結果を利用できます
    func pregMatche(pattern: String, options: NSRegularExpression.Options = [], matches: inout [String]) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: options) else {
            return false
        }
        let targetStringRange = NSRange(location: 0, length: self.count)
        let results = regex.matches(in: self, options: [], range: targetStringRange)
        for i in 0 ..< results.count {
            for j in 0 ..< results[i].numberOfRanges {
                let range = results[i].range(at: j)
                matches.append((self as NSString).substring(with: range))
            }
        }
        return results.count > 0
    }
    
    //正規表現の置換をします
    func pregReplace(pattern: String, with: String, options: NSRegularExpression.Options = []) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: options)
        return regex.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.count), withTemplate: with)
    }
    
    // ローカライズ
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func localized(withTableName tableName: String? = nil, bundle: Bundle = Bundle.main, value: String = "") -> String {
        return NSLocalizedString(self, tableName: tableName, bundle: bundle, value: value, comment: self)
    }
    
}

extension Date {
    func string(_ format:String = "yyyy/MM/dd HH:mm:ss", _ locale:Locale = Locale(identifier: "ja_JP")) -> String {
        let formatter = DateFormatter()
        formatter.locale = locale
        formatter.dateFormat = format
        
        return formatter.string(from: self)
    }
    
    init?(_ dateString:String?, _ format:String = "yyyy/MM/dd HH:mm:ss", _locale:Locale = Locale(identifier: "ja_JP")) {
        
        if dateString == nil {
            return nil
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = format
        guard let date = formatter.date(from: dateString!) else { return nil }
        self = date
    }
}


extension CLLocationDistance {
    
    func toFormatString() -> String {
        
        if self > 5000 {
            return String.init(format: "%dKm", floor(self / 1000))
        }
        else if self > 1100 {
            return String.init(format: "%.1Km", floor(self * 10) / 10000)
        }
        else if self > 1000 {
            return "1Km";
        }
        else if self > 10 {
            return String.init(format: "%dm", floor(self))
        }
        else {
            return "いまここ".localized
        }
    }
}

extension UIViewController {
    
    func navigationBarHidden(hidden: Bool, animated: Bool)
    {
        if let nv = navigationController {
            
            if nv.isNavigationBarHidden == hidden {
                return
            }
            
            if (hidden) {
                // 隠す
                nv.setNavigationBarHidden(hidden, animated: animated)
            } else {
                // 表示する
                nv.setNavigationBarHidden(hidden, animated: animated)
            }
        }
    }

}
