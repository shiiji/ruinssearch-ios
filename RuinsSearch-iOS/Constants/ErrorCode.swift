//
//  ErrorCode.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/27.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit

let APP_ERROR_CODE = 1000

let ERROR_CODE_SOCKET_NOT_FOUND = APP_ERROR_CODE + 1
let ERROR_CODE_SOCKET_DATA_CANNOT_READ = APP_ERROR_CODE + 2
let ERROR_CODE_SOCKET_DATA_WRITE_ERROR = APP_ERROR_CODE + 3
let ERROR_CODE_NO_SELECT_SOCKET = APP_ERROR_CODE + 4
