//
//  MenuViewController.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/20.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import LGSideMenuController

class MenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var menuDef = [
        ["section" : "地図アイテム", "items":
            [
                ["type": "open", "id": "socket_dl", "label": "ソケットダウンロード"],
                ["type": "open", "id": "socket_admin", "label": "ソケットの管理"]
            ]
        ],
        
        ["section" : "設定", "items":
            [
                ["type" : "open", "id": "notification", "label": "通知の設定"],
                ["type" : "setting" , "label": "詳細設定"]
            ]
        ],
        ["section" : "ヘルプ", "items":
            [
                ["type" : "url", "action": "http://freaksmap.4am.jp/manual/", "label":"アプリの使い方"],
                ["type" : "url", "action": "http://freaksmap.4am.jp/contact/", "label":"ご意見やお問い合わせ"],
                ["type" : "url", "action": "http://freaksmap.4am.jp/privacyPolicy/", "label":"プライバシーポリシー"],
                ["type" : "url", "action": "http://freaksmap.4am.jp/termOfService/", "label":"利用規約"]
            ]
        ]
    ]
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.update()
        NotificationCenter.default.addObserver(forName: SETTING_UPDATE, object: nil, queue: OperationQueue.main) { (notification) in
            self.update()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func update() {
        
        self.tableView.reloadData()
        
    }
}


extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuDef.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (menuDef[section]["items"] as AnyObject).count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return menuDef[section]["section"] as? String
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        let section = menuDef[indexPath.section] as Dictionary
        let items = section["items"] as! NSArray
        let item = items.object(at: indexPath.row) as! NSDictionary
        
        cell.textLabel?.text = item.object(forKey: "label") as? String
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.selectionStyle = .default
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.selectionStyle = .none
        
        let section = menuDef[indexPath.section] as Dictionary
        let items = section["items"] as! NSArray
        let item = items.object(at: indexPath.row) as! NSDictionary
        if item["type"] == nil {
            return
        }
        if item["type"] as! String == "open" {
            if item["id"] == nil {
                return
            }
            MainViewController.instance?.openView(item["id"]! as! String)
        }
        if item["type"]! as! String == "setting" {
            if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        
        self.hideLeftView(nil)
    }
}
