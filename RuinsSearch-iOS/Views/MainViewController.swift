//
//  MainViewController.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/19.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftLocation
import Kingfisher
import AudioToolbox

class MainViewController: UIViewController, UIGestureRecognizerDelegate {

    static var instance:MainViewController? = nil

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var opacityView: UIView!
    @IBOutlet weak var opacityLabel: UILabel!
    @IBOutlet weak var opacitySlider: UISlider!
    
    @IBOutlet weak var addLocationButtonView: UIView!
    @IBOutlet weak var trackLocationButtonView: UIView!
    @IBOutlet weak var myLocationButtonView: UIView!
    @IBOutlet weak var trackLocationButton: UIButton!
    
    var customLayer:GMSTileLayer? = nil
    var firstMove:Bool = false
    var selectedMapLayer:Int = 0
    var trackingMode:Bool = false
    
    let mapLayers = [
        ["name":"レイヤー非表示", "base_url":nil, "min_zoom":nil, "max_zoom":nil],
        ["name":"国土地理院地図", "base_url":"https://maps.gsi.go.jp/xyz/std/{z}/{x}/{y}.png", "min_zoom":nil, "max_zoom":nil],
        ["name":"国土地理院地図（淡色）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/pale/{z}/{x}/{y}.png", "min_zoom":nil, "max_zoom":nil],
        ["name":"国土地理院地図（白）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/blank/{z}/{x}/{y}.png", "min_zoom":5.0, "max_zoom":14.0],
        ["name":"写真2004年", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/airphoto/{z}/{x}/{y}.png", "min_zoom":15.0, "max_zoom":18.0],
        ["name":"写真1988年頃（84年版追加）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/gazo4/{z}/{x}/{y}.jpg", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1984年頃（都市圏のみ）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/gazo3/{z}/{x}/{y}.jpg", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1979年頃（都市圏＋α）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/gazo2/{z}/{x}/{y}.jpg", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1974年頃（全国）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/gazo1/{z}/{x}/{y}.jpg", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1961年頃（都市圏＋α）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/ort_old10/{z}/{x}/{y}.png", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1945年頃（都市圏のみ）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/ort_USA10/{z}/{x}/{y}.png", "min_zoom":10.0, "max_zoom":17.0],
        ["name":"写真1936年頃（東京２３区）", "base_url":"https://cyberjapandata.gsi.go.jp/xyz/ort_riku10/{z}/{x}/{y}.png", "min_zoom":13.0, "max_zoom":18.0],
//        ["name":"地図昭和初期（関東のみ）", "base_url":"https://sv53.wadax.ne.jp/~ktgis-net/kjmapw/kjtilemap/kanto/01/{z}/{x}/{y}.png", "min_zoom":8.0, "max_zoom":15.0],
//        ["name":"地図明治大正（関東のみ）", "base_url":"https://sv53.wadax.ne.jp/~ktgis-net/kjmapw/kjtilemap/kanto/00/{z}/{x}/{y}.png", "min_zoom":8.0, "max_zoom":15.0]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        MainViewController.instance = self
        
        mapView.settings.compassButton = true
        mapView.isUserInteractionEnabled = true
        mapView.isMyLocationEnabled = true
        mapView.setMinZoom(4.5, maxZoom: 20)
        mapView.delegate = self
        
        addLocationButtonView.layer.masksToBounds = true
        addLocationButtonView.clipsToBounds = false
        addLocationButtonView.layer.cornerRadius = addLocationButtonView.frame.size.width / 2
        addLocationButtonView.layer.shadowColor = Color.gray.cgColor
        addLocationButtonView.layer.shadowOffset = CGSize(width: 0, height: 1)
        addLocationButtonView.layer.shadowOpacity = 1
        
        myLocationButtonView.layer.masksToBounds = true
        myLocationButtonView.clipsToBounds = false
        myLocationButtonView.layer.cornerRadius = addLocationButtonView.frame.size.width / 2
        myLocationButtonView.layer.shadowColor = Color.gray.cgColor
        myLocationButtonView.layer.shadowOffset = CGSize(width: 0, height: 1)
        myLocationButtonView.layer.shadowOpacity = 1
        
        trackLocationButtonView.layer.masksToBounds = true
        trackLocationButtonView.clipsToBounds = false
        trackLocationButtonView.layer.cornerRadius = addLocationButtonView.frame.size.width / 2
        trackLocationButtonView.layer.shadowColor = Color.gray.cgColor
        trackLocationButtonView.layer.shadowOffset = CGSize(width: 0, height: 1)
        trackLocationButtonView.layer.shadowOpacity = 1

        addObserver(mapView, forKeyPath: "myLocation", options: [.new, .old], context: nil)
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (_) in
            if self.trackingMode && self.mapView.myLocation != nil {
                print(self.mapView.myLocation)
                self.mapView.animate(to: GMSCameraPosition(target: self.mapView.myLocation!.coordinate, zoom: self.mapView.camera.zoom, bearing: self.mapView.myLocation!.course, viewingAngle: self.mapView.camera.viewingAngle))
            }
        }
        
        NotificationCenter.default.addObserver(forName: LMN_UPDATED, object: nil, queue: .main) { (_) in
            if LocationManager.shared.last != nil && (LocationManager.shared.last?.horizontalAccuracy ?? 10000) < 800.0 && !self.firstMove {
                self.firstMove = true
                let camera = GMSCameraPosition.camera(withTarget: LocationManager.shared.last!.coordinate, zoom: 13)
                self.mapView.animate(to: camera)
            }
        }
        
        NotificationCenter.default.addObserver(forName: KNOCK_FINISHED, object: nil, queue: .main) { (_) in
            if KnockManager.shared.knockCount == 3 {
                AudioServicesPlaySystemSound(1075)
                let coord = LocationManager.shared.last?.coordinate
                print(coord?.latitude, coord?.longitude)
            }
        }
        
        let tapgestureRecongnizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        tapgestureRecongnizer.delegate = self
        self.view.addGestureRecognizer(tapgestureRecongnizer)

        KnockManager.shared.start()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        firstMove = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if object_getClassName(object) == object_getClassName(mapView) && keyPath == "myLocation" {
            print("move")
        }
        
    }
    
    // MARK: Public
    func openView(_ viewId:String) {
        
    }
    
    // MARK: Private
    func updateMap() {
        if customLayer != nil {
            customLayer!.map = nil
            customLayer = nil
        }
        
        let val = mapLayers[selectedMapLayer];
        if val["base_url"] as? String != nil {
            customLayer = TileLayer(baseUrl: val["base_url"] as! String)
            customLayer!.opacity = 1 - opacitySlider.value
            customLayer!.map = mapView
            opacityView.isHidden = false
            mapView.setMinZoom(val["min_zoom"] as? Float ?? 9, maxZoom: val["max_zoom"] as? Float ?? 16.7)
        }
        else {
            opacityView.isHidden = true
            mapView.setMinZoom(4.5, maxZoom: 20)
        }
    }
    
    // MARK: Action
    @IBAction func onMapStyle(_ sender: Any) {
        
        let ac = UIAlertController(title: "マップスタイルの選択", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "デフォルト", style: .default, handler: { (_) in
            self.mapView.mapStyle = nil
            self.mapView.mapType = .normal
        }))
        ac.addAction(UIAlertAction(title: "衛星写真", style: .default, handler: { (_) in
            self.mapView.mapType = .hybrid
        }))
        ac.addAction(UIAlertAction(title: "黒地図", style: .default, handler: { (_) in
            self.mapView.mapType = .normal
            if let styleUrl = Bundle.main.url(forResource: "map_style_dark", withExtension: "json") {
                self.mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleUrl)
            }
        }))
        ac.addAction(UIAlertAction(title: "白地図", style: .default, handler: { (_) in
            self.mapView.mapType = .normal
            if let styleUrl = Bundle.main.url(forResource: "map_style_silver", withExtension: "json") {
                self.mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleUrl)
            }
        }))
        
        self.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func onLayer(_ sender: Any) {
        let ac = UIAlertController(title: "重ねレイヤーの選択", message: nil, preferredStyle: .alert)
        
        mapLayers.forEach { (value) in
            if value["name"] != nil {
                let action = UIAlertAction(title: (value["name"]! as! String), style: .default, handler: { (a) in
                    /*
                    self.selectedMapLayer = a.value(forKey: "key") as! Int
                    self.updateMap()
 */
                    if let index = ac.actions.index(of: a) {
                        self.selectedMapLayer = index
                        self.updateMap()
                    }
                })
                ac.addAction(action)
            }
        }
        
        ac.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
        
        self.present(ac, animated: true, completion: nil)
    }
    
    @objc func tapped(_ sender: UITapGestureRecognizer){
        if sender.state == .ended {
            print("タップ")
        }
    }
    
    @IBAction func onOpacityChange(_ sender: Any) {
        if customLayer != nil {
            customLayer!.opacity = 1 - opacitySlider.value
            let value = opacitySlider.value * 100.0
            opacityLabel.text = String(format:"%.0f%%", floor(value))
        }
    }

    @IBAction func onAddLocation(_ sender: Any) {
    }
    
    @IBAction func onMyLocation(_ sender: Any) {
        if mapView.myLocation != nil {
            let camera = GMSCameraUpdate.setTarget(mapView.myLocation!.coordinate, zoom: mapView.camera.zoom < 13 ? 13 : mapView.camera.zoom)
            mapView.animate(with: camera)
        }
    }
    
    @IBAction func onTrackLocation(_ sender: Any) {
        trackingMode = !trackingMode
        trackLocationButton.setImage(UIImage(named: "Icon_Tracking" + (trackingMode ? "On" : "Off")), for: .normal)
    }
}

extension MainViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("zoom = %", position.zoom)
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("position")
    }
    
}

// MARK: 特殊なタイルレイヤークラス
class TileLayer:GMSTileLayer {
    
    var _baseUrl:String = ""
    
    init(baseUrl:String) {
        super.init()
        _baseUrl = baseUrl
    }
    
    override func requestTileFor(x: UInt, y: UInt, zoom: UInt, receiver: GMSTileReceiver) {
//        let url = URL(string:String(format: _baseUrl, zoom, x, y))
        let str = _baseUrl.replace("{z}", String(zoom)).replace("{x}", String(x)).replace("{y}", String(y))
//        print(str)
        if let url = URL(string: str) {
            ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) {
                (image, error, url, data) in
                receiver.receiveTileWith(x: x, y: y, zoom: zoom, image: image)
            }
        }
    }
    
}
