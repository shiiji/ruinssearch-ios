//
//  SocketManager.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/27.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseAuth
import CryptoSwift

class SocketManager: NSObject {

    static let shared:SocketManager = SocketManager()
    
    var loading:Bool = false
    var currentSocket:SocketModel? = nil
    var currentSocketCategories:[SocketCategoryModel]? = nil
    
    static func remoteList(_ timeout:TimeInterval = 30, block:@escaping([SocketModel]?) -> Void) {
        
    }
    
    static func subscribe(_ socketId:Int, block:@escaping(Bool, NSError?) -> Void) {
        
    }
    
    static func sync(_ socketId:Int, block:@escaping(Bool, NSError?) -> Void) {
        
    }
    
    static func syncAll(block:@escaping(Bool, NSError?) -> Void) {
        
    }
    
    func open(_ socketId:String, block:@escaping(Bool, NSError?) -> Void) {
        
        if currentSocket?.id == socketId {
            block(true, nil)
            loading = false
        }
        
        loading = true
        close()
        
        DispatchQueue.global(qos: .default).async {
            
            guard let dPath = self.searchSocketFilePath(socketId) else {
                block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_SOCKET_NOT_FOUND, userInfo: nil))
                return
            }
            guard let cPath = self.searchSocketCategoryFilePath(socketId) else {
                block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_SOCKET_NOT_FOUND, userInfo: nil))
                return
            }
            
            do {
                let sdata = try Data(contentsOf: URL(fileURLWithPath: dPath))
                let socket = try JSONDecoder().decode(SocketModel.self, from: sdata)
                
                let cdata = try Data(contentsOf: URL(fileURLWithPath: cPath))
                let category = try JSONDecoder().decode(Array<SocketCategoryModel>.self, from: cdata)
                
                self.currentSocket = socket
                self.currentSocketCategories = category
                
                self.loading = false
                block(true, nil)
              }
            catch {
                self.loading = false
                block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_SOCKET_NOT_FOUND, userInfo: nil))
            }
        }
    }
    
    func close() {
        currentSocket = nil
    }
    
    func saveSocket(_ socket:SocketModel, block:@escaping(Bool, NSError?) -> Void) {
        
        DispatchQueue.global(qos: .default).async {
            
            if let path = self.searchSocketFilePath(socket.id, false) {
                
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(socket)
                    try data.write(to: URL(fileURLWithPath: path), options: [])
                    block(true, nil)
                }
                catch {
                    block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_SOCKET_DATA_WRITE_ERROR, userInfo: nil))
                }
                
            }
            else {
                block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_SOCKET_DATA_WRITE_ERROR, userInfo: nil))
            }
        }
        
    }
    
    func newArticle() -> SocketArticleModel? {
        
        if currentSocket == nil {
            return nil
        }
        
        var article = SocketArticleModel()
        article.socketId = currentSocket!.id
        article.owner = Auth.auth().currentUser?.uid
        return article
    }
    
    func addArticle(_ article:SocketArticleModel, withSave:Bool = false, block:@escaping(Bool, NSError?) -> Void) {
        
        var nArticle = article
        nArticle.owner = Auth.auth().currentUser?.uid
        nArticle.updatedAt = Date()
        
        if nArticle.id == nil {
            nArticle.id = generateNewArticleId("article")
        }

        if currentSocket == nil {
            block(false, NSError(domain: Bundle.main.bundleIdentifier!, code: ERROR_CODE_NO_SELECT_SOCKET, userInfo: nil))
            return
        }
        
        currentSocket!.articles[nArticle.id!] = nArticle
        
        if withSave {
            saveSocket(currentSocket!) { (b, e) in
                block(b, e)
            }
        }
        else {
            block(true, nil)
        }
    }
    
    func editArticle(_ article:SocketArticleModel, block:@escaping(Bool, NSError?) -> Void) {
        addArticle(article) { (b, e) in
            block(b, e)
        }
    }
    
    func nearestArticles(_ socketId:Int, _ location:CLLocation, block:@escaping([(String,SocketArticleModel)]?, NSError?) -> Void) {
        
        let sorted = currentSocket?.articles.sorted(by: { (value1, value2) -> Bool in

            var location1:CLLocation? = nil
            var location2:CLLocation? = nil
            
            if value1.value.latitude != nil && value1.value.longitude != nil {
                location1 = CLLocation(latitude: value1.value.latitude!, longitude: value1.value.longitude!)
            }
            if value2.value.latitude != nil && value2.value.longitude != nil {
                location2 = CLLocation(latitude: value2.value.latitude!, longitude: value2.value.longitude!)
            }
            
            if location1 != nil && location2 != nil {
                return (location.distance(from: location1!) < location.distance(from: location2!))
            }
            if location1 == nil {
                return false
            }
            if location2 == nil {
                return true
            }
            
            return true
            
        })
        
        block(sorted, nil)
    }

    private func generateNewArticleId(_ type:String) -> String {
        return String(format: "%s_%s", type, Date().string()).sha1()
    }
    
    private func searchSocketCategoryFilePath(_ socketId:String?, _ checkExists:Bool = true) -> String? {
        
        if socketId == nil {
            return nil
        }
        
        let socketDirectoryPath = NSHomeDirectory() + "/Library/Sockets/"
        var isDirectory:ObjCBool = true
        var isFile:ObjCBool = false
        if !FileManager.default.fileExists(atPath: socketDirectoryPath, isDirectory: &isDirectory) {
            try! FileManager.default.createDirectory(atPath: socketDirectoryPath, withIntermediateDirectories: true, attributes: nil)
        }
        let socketFilePath = String(format: "%s/category_%s.dat", socketDirectoryPath, socketId!)
        if checkExists && !FileManager.default.fileExists(atPath: socketFilePath, isDirectory: &isFile) {
            return nil
        }
        return socketFilePath
    }
    
    private func searchSocketFilePath(_ socketId:String?, _ checkExists:Bool = true) -> String? {
        
        if socketId == nil {
            return nil
        }
        
        let socketDirectoryPath = NSHomeDirectory() + "/Library/Sockets/"
        var isDirectory:ObjCBool = true
        var isFile:ObjCBool = false
        if !FileManager.default.fileExists(atPath: socketDirectoryPath, isDirectory: &isDirectory) {
            try! FileManager.default.createDirectory(atPath: socketDirectoryPath, withIntermediateDirectories: true, attributes: nil)
        }
        let socketFilePath = String(format: "%s/data_%s.dat", socketDirectoryPath, socketId!)
        if checkExists && !FileManager.default.fileExists(atPath: socketFilePath, isDirectory: &isFile) {
            return nil
        }
        return socketFilePath
    }
}
