//
//  KnockManager.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/20.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation

class KnockManager: NSObject {
    
    static let shared:KnockManager = KnockManager()
    
    let manager = CMMotionManager()
    var knockCount:Int = 0
    let motionInterval:Double = 0.01
    var knockWaitTimer:Timer?
    var knockWaitInterval:Double = 2
    var knockStartLocation:CLLocation?
    private let lm = CLLocationManager()

    var previosAccelerationZTime:Date = Date()
    var previosAccelerationZ:Double = 0.0
    
    func start() {
        
        lm.delegate = self
        lm.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        manager.deviceMotionUpdateInterval = motionInterval
        
        if manager.isDeviceMotionAvailable {
        
            manager.startDeviceMotionUpdates(to: OperationQueue.current!) { (data:CMDeviceMotion!, error) in
                
                if data.userAcceleration.z  - self.previosAccelerationZ > 0.40 {
                    print(data.userAcceleration.z - self.previosAccelerationZ, Date().timeIntervalSince(self.previosAccelerationZTime))
                    if self.knockCount == 0 {
                        print("First Knock")
                        self.knockCount = 1
                        self.lm.requestLocation()
                        NotificationCenter.default.post(name: KNOCK_STARTED, object: nil)
                    }
                    else if Date().timeIntervalSince(self.previosAccelerationZTime) > 0.1 && Date().timeIntervalSince(self.previosAccelerationZTime) < 0.4 {
                        self.knockCount = self.knockCount + 1
                        print("Knock count = ", self.knockCount, ", interval = ", self.previosAccelerationZTime.timeIntervalSince(Date()))
                    }
                    NotificationCenter.default.post(name: KNOCK_UPDATED, object: nil)
                    
                    if self.knockWaitTimer != nil {
                        self.knockWaitTimer?.invalidate()
                        self.knockWaitTimer = nil
                    }
                    self.knockWaitTimer = Timer.scheduledTimer(withTimeInterval: self.knockWaitInterval, repeats: false, block: { (_) in
                        print("Knock Finished (count = ", self.knockCount)
                        NotificationCenter.default.post(name: KNOCK_FINISHED, object: nil)
                        self.knockCount = 0
                    })
                    self.previosAccelerationZTime = Date()
                }
                self.previosAccelerationZ = data.userAcceleration.z
                
            }
        }
    }
    
}

extension KnockManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.knockStartLocation = locations.first
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("request location fail", error.localizedDescription)
    }
}
