//
//  LocationManager.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/19.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject {
    
    static let shared:LocationManager = LocationManager()
    
    var last:CLLocation? = nil
    let lm:CLLocationManager = CLLocationManager()
    
    private var pTrackingMode:Bool = false
    
    override init() {
        super.init()
        checkAuthorizationStatus()
    }

    func checkAuthorizationStatus() {
        DispatchQueue.global(qos: .default).sync {
            sleep(3)
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            case .denied:
                break
            case .notDetermined:
                DispatchQueue.main.async {
                    self.lm.requestAlwaysAuthorization()
                }
                break
            case .restricted:
                break
            }
        }
    }
    
    func start() {
        lm.delegate = self
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.distanceFilter = 50
        lm.startUpdatingLocation()
    }
    
    func stop() {
        lm.stopUpdatingLocation()
    }
    
    func restart() {
        stop()
        start()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        last = locations.last
        NotificationCenter.default.post(name: LMN_UPDATED, object: ["location":last])
    }
    
}
