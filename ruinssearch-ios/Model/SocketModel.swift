//
//  SocketModel.swift
//  ruinssearch-ios
//
//  Created by kazama on 2018/05/27.
//  Copyright © 2018年 sera. All rights reserved.
//

import UIKit
import CoreLocation

struct SocketModel: Codable {
    var id:String? = nil
    var name:String? = nil
    var memo:String? = nil
    var introUrl:String? = nil
    var owner:String? = nil
    var termsOfService:String? = nil
    var sort:Int = 0
    var imageUrl:String? = nil
    var agreed:Bool = false
    var articles:[String:SocketArticleModel] = [:]
    var isPrivate:Bool = true
}

struct SocketArticleModel: Codable {
    
    var id:String? = nil
    var socketId:String? = nil
    var categoryId:Int = 0
    var name:String? = nil
    var memo:String? = nil
    var nameIndex:Int = 0
    var country:String = "JPN"
    var address1:String? = nil
    var address2:String? = nil
    var address3:String? = nil
    var latitude:Double? = nil
    var longitude:Double? = nil
    var stateId:Int = 0
    var visited:[Date] = []
    var parameters:[String:String] = [:]
    var createdUserId:Int = 0
    var imageUrl:String? = nil
    var images:[String] = []
    var owner:String? = nil
    var shareExpired:Date? = nil
    var deleted:Bool = false
    var updatedAt:Date? = nil
    var isPrivate:Bool = true
}

struct SocketCategoryModel: Codable {
    
    var id:String? = nil
    var name:String? = nil
    var nameIndex:Int = 0
    var socketId:String? = nil
    var sort:Int = 0
    var visible:Bool = true
    
}
